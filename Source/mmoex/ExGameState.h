// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include <unordered_map>
#include <memory>
#include "Ball.h"
#include "ExGameState.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChatMessageSend, const FString&, msg);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChatMessageRecv, const FString&, msg);
/**
 * 
 */
UCLASS()
class MMOEX_API AExGameState : public AGameState
{
	GENERATED_BODY()

public:
	AExGameState();

	void Tick(float delta) override;
	
	UPROPERTY(BlueprintAssignable)
	FOnChatMessageRecv OnChatMessageRecv;
	void BeginPlay() override;
	void RecvTcpMessage();
	void RecvUdpMessage();
	UFUNCTION(BlueprintCallable,Category="Client")
	void SendChatMessage(const FString& msg);
	void RecvChatMessage(std::shared_ptr<uint8> buf,uint32 length);
	std::unordered_map<std::string, ABall*> balls;
private:
	FTimerHandle RecvUdpHandle;
	FTimerHandle RecvTcpHandle;
	
	
};
