// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include <string>
#include "Ball.generated.h"

UCLASS()
class MMOEX_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABall();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	USphereComponent* getRoot()const { return SphereComponent; }
	FString Nick;
	std::string id;
	void LifeTimeReset();
	void Destroy();
	
private:
	USphereComponent* SphereComponent;
	FTimerHandle lifetime;

	USpringArmComponent* SpringArmComponent;

	UCameraComponent* CameraComponent;
	UStaticMeshComponent* StaticMeshComponent;
	
};
