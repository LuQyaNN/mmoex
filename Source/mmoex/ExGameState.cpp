// Fill out your copyright notice in the Description page of Project Settings.

#include "mmoex.h"
#include "ExGameInstance.h"
#include <memory>

#include "ExGameState.h"

float uint8Tofloat(uint8* bytes);
AExGameState::AExGameState() {
	
}

void AExGameState::Tick(float delta) {
	Super::Tick(delta);

}

void AExGameState::BeginPlay() {
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(RecvUdpHandle, this, &AExGameState::RecvUdpMessage, 0.0003f, true);
	GetWorld()->GetTimerManager().SetTimer(RecvTcpHandle, this, &AExGameState::RecvTcpMessage, 0.0003f, true);

}

void AExGameState::RecvUdpMessage() {
	uint32 pending_data;
	UExGameInstance* inst = (UExGameInstance*)GetGameInstance();
	auto udp_socket = inst->udp_socket;
	if ( !udp_socket->HasPendingData(pending_data))return;

	std::shared_ptr<uint8> buf(new uint8[pending_data], [](uint8* p) {delete[] p; });
	int32 bytes_recv;

	udp_socket->Recv(buf.get(), pending_data, bytes_recv);

	switch (buf.get()[0]) {
	case 2: {
		uint8* pos_in_buf= buf.get();
		uint8 clients_num = buf.get()[1];
		pos_in_buf += 2;//first client begin
		for(int num = 0;num<clients_num;++num){
			uint8 nick_size = *pos_in_buf;
			pos_in_buf++;
		std::string nick((char*)(pos_in_buf), nick_size);
		pos_in_buf+= nick_size;
		
		std::string id_str((char*)(pos_in_buf), 16);
		pos_in_buf+=16;
		auto ball = balls.find(id_str);
		if (ball != balls.end()) {
			
			GEngine->AddOnScreenDebugMessage(0, 1, FColor::White, FString("info another client recieved"));
		
			
			FVector		tmp_vel;
			tmp_vel.X = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			tmp_vel.Y = uint8Tofloat(pos_in_buf ); pos_in_buf += 4;
			tmp_vel.Z = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			
			FVector tmp_avel;
			tmp_avel.X = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			tmp_avel.Y = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			tmp_avel.Z = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			FRotator tmp_rot;
			tmp_rot.Yaw = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			tmp_rot.Pitch = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			tmp_rot.Roll = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			FVector	tmp_loc;
			tmp_loc.X = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			tmp_loc.Y = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			tmp_loc.Z = uint8Tofloat(pos_in_buf); pos_in_buf += 4;
			
			USphereComponent* ballroot = ball->second->getRoot();
			
			
			FRigidBodyState rbs;
			rbs.Flags = ERigidBodyFlags::NeedsUpdate;
			rbs.AngVel = tmp_avel;
			rbs.LinVel = tmp_vel;
			rbs.Position = tmp_loc;
			rbs.Quaternion = tmp_rot.Quaternion();
			FRigidBodyErrorCorrection fbec;
			FVector odp;
			
			ball->second->getRoot()->ConditionalApplyRigidBodyState(rbs, fbec, odp);
			ball->second->LifeTimeReset();
			
		} else {
			
			ABall* ball = (ABall*)GetWorld()->SpawnActor(ABall::StaticClass());
			ball->Nick = FString(nick.c_str());
			ball->id = id_str;
			balls.insert(std::make_pair(id_str, ball));
		}
		}
		break;
	}
	}
}

void AExGameState::SendChatMessage(const FString& msg) {
	
	UExGameInstance* inst = (UExGameInstance*)GetGameInstance();
	auto tcp_socket = inst->tcp_socket;
	std::string utf8_msg(TCHAR_TO_UTF8(*msg));
	
	if(utf8_msg.size()==0)return;
	utf8_msg.append(1, '\0');
	std::shared_ptr<uint8> data_buffer(new uint8[utf8_msg.size()+1], [](uint8* p) {delete[] p; });
	data_buffer.get()[0]=3;
	for (int i = 0; i < utf8_msg.size(); ++i) {
		data_buffer.get()[1+i]= utf8_msg[i];
	}
	int32 bytessend =0;
	tcp_socket->Send(data_buffer.get(), utf8_msg.size() + 1, bytessend);
	
	//for()
	//tcp_socket
}

void AExGameState::RecvChatMessage(std::shared_ptr<uint8> buf, uint32 length) {
	uint8 nick_size = buf.get()[1];
	FString msg(UTF8_TO_TCHAR(buf.get()));
	msg.RemoveAt(0,  2);
	msg.InsertAt(nick_size,':');

	
	OnChatMessageRecv.Broadcast(msg);
}

void AExGameState::RecvTcpMessage() {

	uint32 pending_data_size;
	UExGameInstance* inst = (UExGameInstance*)GetGameInstance();
	auto tcp_socket = inst->tcp_socket;
	if (!tcp_socket->HasPendingData(pending_data_size))return;
	std::shared_ptr<uint8> data_buffer(new uint8[pending_data_size], [](uint8* p) {delete[] p; });
	int32 bytes_read;
	tcp_socket->Recv(data_buffer.get(), pending_data_size, bytes_read);

	switch (data_buffer.get()[0]) {
	case 1: {
		for (int i = 0; i < 16; ++i) {
			inst->id[i] = data_buffer.get()[1 + i];
		}
		GEngine->AddOnScreenDebugMessage(1, 1, FColor::White, FString("id recieved"));
		break;
	}
	case 3: {
		RecvChatMessage(data_buffer, pending_data_size);
	}
	
	}



}

