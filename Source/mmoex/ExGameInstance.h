// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "Networking.h"
#include <unordered_map>
#include "Ball.h"
#include "ExGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnConnectError,const FString&, error_str);
/**
 * 
 */
UCLASS()
class MMOEX_API UExGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable,category="Server")
	bool ConnectTo(const FString& address);


	UPROPERTY(BlueprintAssignable)
		FOnConnectError OnConnectError;

	bool SendNick(FString msg);
	

	void SendRepMovement(FRepMovement& rep);
	TSharedPtr<FSocket> udp_socket;

	TSharedPtr<FSocket> tcp_socket;
	FIPv4Endpoint server_ep;
	UFUNCTION(BlueprintCallable, category = "Server")
		void SetNick(const FString& nick);
	uint8 id[16];
private:
	
	
	
	FString Nick;
	
	
	
	
};
