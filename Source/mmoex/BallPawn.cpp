// Fill out your copyright notice in the Description page of Project Settings.

#include "mmoex.h"
#include "ExGameInstance.h"
#include "BallPawn.h"



// Sets default values
ABallPawn::ABallPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> mat(TEXT("Material'/Game/mt1.mt1'"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> mesh(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationPitch = true;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	RootComponent = SphereComponent;
	SphereComponent->SetSphereRadius(50);
	
	
	SphereComponent->SetCollisionProfileName("Pawn");
	SphereComponent->SetVisibility(true);
	SphereComponent->SetEnableGravity(true);
	SphereComponent->SetSimulatePhysics(true);
	SphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SphereComponent->SetNotifyRigidBodyCollision(true);
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->AttachTo(RootComponent);
	StaticMeshComponent->SetStaticMesh(mesh.Object);
	StaticMeshComponent->SetMaterial(0, mat.Object);
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->AttachTo(SphereComponent);
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->TargetArmLength = 1000;
	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->AttachTo(SpringArmComponent, SpringArmComponent->GetAttachSocketName());

	
}

// Called when the game starts or when spawned
void ABallPawn::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(SendMoveTimer, this, &ABallPawn::SendMove, 0.1f, true);
}

void ABallPawn::SendMove() {
	FRigidBodyState tmp_state;
	SphereComponent->GetRigidBodyState(tmp_state);
	ReplicatedMovement.FillFrom(tmp_state);
	
	Cast<UExGameInstance>(GetGameInstance())->SendRepMovement(ReplicatedMovement);
}

// Called every frame
void ABallPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	
}

// Called to bind functionality to input
void ABallPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAxis("MoveForward", this, &ABallPawn::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ABallPawn::MoveRight);
	InputComponent->BindAxis("CamUp", this, &ABallPawn::CamUp);
	InputComponent->BindAxis("CamRight", this, &ABallPawn::CamRight);
	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &ABallPawn::Jump);
}

void ABallPawn::MoveForward(float val) {
	if (val != 0) {
		SphereComponent->AddTorque(val*CameraComponent->GetRightVector() * 10000000);
		
	}
}

void ABallPawn::MoveRight(float val) {
	if (val != 0) {
		FVector right = CameraComponent->GetForwardVector();
		right.Z = 0;
		SphereComponent->AddTorque(-val*right * 10000000);
	}
}

void ABallPawn::CamUp(float val) {
	if (val != 0) {
		Controller->SetControlRotation(Controller->GetControlRotation()+ FRotator( val,0, 0)*GetWorld()->GetDeltaSeconds() * 300);
		
	}
}

void ABallPawn::CamRight(float val) {
	if (val != 0) {
		Controller->SetControlRotation(Controller->GetControlRotation() + FRotator(0, val, 0)*GetWorld()->GetDeltaSeconds() * 300);
		
	}
}

void ABallPawn::Jump() {
	
		FHitResult hit;
		FCollisionQueryParams qp("t", false, this);
		GetWorld()->LineTraceSingleByChannel(hit, GetActorLocation(), GetActorLocation() - FVector(0, 0, 100.f), ECollisionChannel::ECC_GameTraceChannel1, qp);
		if (hit.IsValidBlockingHit()) {
			const FVector Impulse = FVector(0.f, 0.f, 200000);
			SphereComponent->AddImpulse(Impulse);
			
		}
		
	
}

void ABallPawn::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) {
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	
}

