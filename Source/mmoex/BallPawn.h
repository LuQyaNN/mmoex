// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "BallPawn.generated.h"

UCLASS()
class MMOEX_API ABallPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABallPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	void MoveForward(float val);
	void MoveRight(float val);
	void CamUp(float val);
	void CamRight(float val);
	void Jump();


	USphereComponent* SphereComponent;

	USpringArmComponent* SpringArmComponent;
	
	UCameraComponent* CameraComponent;
	UStaticMeshComponent* StaticMeshComponent;

	void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)override;

	void SendMove();
private:
	bool bCanJump = true;
	FTimerHandle SendMoveTimer;
	
};
