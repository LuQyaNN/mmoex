// Fill out your copyright notice in the Description page of Project Settings.

#include "mmoex.h"
#include "ExGameState.h"
#include "Ball.h"


// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> mat(TEXT("Material'/Game/mt1.mt1'"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> mesh(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));

	
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	RootComponent = SphereComponent;
	SphereComponent->SetSphereRadius(50);


	SphereComponent->SetCollisionProfileName("Pawn");
	SphereComponent->SetVisibility(true);
	SphereComponent->SetEnableGravity(true);
	SphereComponent->SetSimulatePhysics(true);
	SphereComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SphereComponent->SetNotifyRigidBodyCollision(true);
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->AttachTo(RootComponent);
	StaticMeshComponent->SetStaticMesh(mesh.Object);
	StaticMeshComponent->SetMaterial(0, mat.Object);
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->AttachTo(SphereComponent);
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->TargetArmLength = 1000;
	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->AttachTo(SpringArmComponent, SpringArmComponent->GetAttachSocketName());
	
}

void ABall::LifeTimeReset() {
	
	GetWorld()->GetTimerManager().SetTimer(lifetime, this,&ABall::Destroy, 1.6f, false, 1.6f);
}

void ABall::Destroy() {
	((AExGameState*)GetWorld()->GetGameState())->balls.erase(id);
	Super::Destroy();
}



// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABall::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

