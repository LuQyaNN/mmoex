// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "mmoexGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MMOEX_API AmmoexGameMode : public AGameMode
{
	GENERATED_BODY()
public:
	AmmoexGameMode();
	void BeginPlay()override;
	
	
	
};
