// Fill out your copyright notice in the Description page of Project Settings.

#include "mmoex.h"
#include <memory>
#include "Ball.h"

#include "ExGameInstance.h"


float uint8Tofloat(uint8* bytes) {
	return *reinterpret_cast<float*>(bytes);
}

uint8* floatTouint8(float* fl) {
	return reinterpret_cast<uint8*>(fl);
}

bool UExGameInstance::ConnectTo(const FString& address) {
	if(!FIPv4Endpoint::Parse(address, server_ep))return false;
	FTcpSocketBuilder TcpSocketBuilder("TcpSocketBuilder");
	tcp_socket = MakeShareable<FSocket>(TcpSocketBuilder.AsBlocking().Build());
	if (!tcp_socket->Connect(server_ep.ToInternetAddr().Get())) {
		return false;
		
	}
	
	FUdpSocketBuilder UdpSocketBuilder("UdpSocketBuilder");
	udp_socket= MakeShareable<FSocket>(UdpSocketBuilder.AsBlocking().BoundToPort(tcp_socket.Get()->GetPortNo()).Build());
	
	
	if(Nick.Len() ==0)return false;
	
	
	SendNick( Nick);
	return true;
	
}

void UExGameInstance::SetNick(const FString& nick) {
	Nick=nick;
}

bool UExGameInstance::SendNick( FString msg) {
	int32 buf_size = msg.Len() + 2;
	std::shared_ptr<uint8> buf(new uint8[buf_size], [](uint8* p) {delete[] p; });
	buf.get()[0] = 1;
	char* tmp_ansi = TCHAR_TO_ANSI(*msg);
	for (int32 i = 0; i < msg.Len()+1; ++i) {
		
		buf.get()[i + 1] = tmp_ansi[i];
	}
	int32 tmpsent;
	if(tcp_socket->Send(buf.get(), buf_size, tmpsent))return true;
	return false;
}

void UExGameInstance::SendRepMovement(FRepMovement& rep) {
	if(!udp_socket.IsValid()&& udp_socket->GetConnectionState() != 1)return;
	GEngine->AddOnScreenDebugMessage(65, 1, FColor::White, FString("rep move"));
	std::shared_ptr<uint8> buf(new uint8[65], [](uint8* p) {delete[] p; });
	int32 bytes_send;
	buf.get()[0] = 2;
	for (int i = 0; i < 16; ++i) {
		buf.get()[1 + i] = id[i];
	}
	uint8* vel_x = floatTouint8(&rep.LinearVelocity.X);
	uint8* vel_y = floatTouint8(&rep.LinearVelocity.Y);
	uint8* vel_z = floatTouint8(&rep.LinearVelocity.Z);
	uint8* avel_x = floatTouint8(&rep.AngularVelocity.X);
	uint8* avel_y = floatTouint8(&rep.AngularVelocity.Y);
	uint8* avel_z = floatTouint8(&rep.AngularVelocity.Z);
	uint8* rot_yaw = floatTouint8(&rep.Rotation.Yaw);
	uint8* rot_pitch = floatTouint8(&rep.Rotation.Pitch);
	uint8* rot_roll = floatTouint8(&rep.Rotation.Roll);
	uint8* loc_x = floatTouint8(&rep.Location.X);
	uint8* loc_y = floatTouint8(&rep.Location.Y);
	uint8* loc_z = floatTouint8(&rep.Location.Z);
	for (int i = 0; i < 4; ++i) {
		buf.get()[17 + i] = vel_x[i];
		buf.get()[21 + i] = vel_y[i];
		buf.get()[25 + i] = vel_z[i];
		buf.get()[29 + i] = avel_x[i];
		buf.get()[33 + i] = avel_y[i];
		buf.get()[37 + i] = avel_z[i];
		buf.get()[41 + i] = rot_yaw[i];
		buf.get()[45 + i] = rot_pitch[i];
		buf.get()[49 + i] = rot_roll[i];
		buf.get()[53 + i] = loc_x[i];
		buf.get()[57 + i] = loc_y[i];
		buf.get()[61 + i] = loc_z[i];

	}

	if (!udp_socket->SendTo(buf.get(), 65, bytes_send, server_ep.ToInternetAddr().Get())) {
		GEngine->AddOnScreenDebugMessage(0, 20, FColor::White, FString("udp_socket->SendTo error"));
	}
}








